#include <stdio.h>

/**
 * Estudiante: Andrés Lopez
 * Carnet: 2019160378
 */
struct estudiante{
    char nombre[10];
    int carnet;
};

struct estudiante estudiantes[10]; //Se declara el arreglo de struct de estudiante

int main(){
    
    for (int i = 0; i < 10; i++) {
		//Se llena el arreglo "estudiantes" con información ingresada por el usuario 
        printf("%d",i);
        printf(" Escriba el nombre del estudiante: ");
        scanf("%s", &estudiantes[i].nombre);
        
        printf(" Escriba el carnet del estudiante: ");
        scanf("%d", &estudiantes[i].carnet);
    }
    
    /**
    for (int i = 0; i < 10; i++) {
        printf("%s", estudiantes[i].nombre);
        printf("%d\n", estudiantes[i].carnet);
    }
    **/
    int ind_estudiante;
    printf("Que posicion de carnet desea validar ?\n");
    scanf("%d",&ind_estudiante);
    
    int carnet_usuario;
    printf("Cuál es el carnet del estudiante en la pos %d\n",ind_estudiante);
    scanf("%d",&carnet_usuario);
    
    if(carnet_usuario == estudiantes[ind_estudiante].carnet){
        printf("El carnet es correcto\n");
        printf("%d",estudiantes[ind_estudiante].carnet);
    }
    else{
        printf("El carnet ingresado no corresponde con la posicion %d", ind_estudiante);
    }
    
    return 0;
}
